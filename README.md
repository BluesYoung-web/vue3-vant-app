# Vue3 + Vant + Typescript + Vite2

- 基于 vue3 + vant + typescript + vite2 的移动端app模板
- 方法自动按需导入(基于[unplugin-auto-import](https://github.com/antfu/unplugin-auto-import))
- 组件自动按需导入(基于[unplugin-vue-components](https://github.com/antfu/unplugin-vue-components))
- <del>**使用 windicss，零配置自动 css 摇树优化**</del>
- **使用原子化的 `unocss`，根据类名自动生成对应的样式**
- **拥有自动创建页面的 node 脚本，实现页面创建及路由注册自动化**

## 环境

推荐 node 16+(lts)

## 权限管理

- 用户必须登录之后才能进行操作
- 否则只能访问[路由白名单](./src/route/index.ts)中的路径

## 前置条件

- 在项目根目录下新建 `.env.local` 文件，内容如下:

```bash
# ！！！ 变量名必须以 VITE_ 开头
# 使用 import.meta.变量名 获取

# 接口请求地址，后端使用 nginx 代理来跨域时，使用绝对路径
VITE_BASE_HTTP = '/api'
# 启用 VConsole 控制台(按需启用，生产环境无需配置)
VITE_ENABLE_VCONSOLE = true
```

## 使用

```bash
# 装依赖
yarn
# 本地运行
yarn dev
# 打包
yarn build # 限制运行内存
yarn build:prod # 不限制运行内存
yarn pre # 预览打包后的文件
```

### <span style="color: red">自动创建页面</span>

- 快捷指令 `yarn create-new-page`
- 命令行：
  - <span style="color: red">装完依赖之后，需要执行 `npm link ./bin`</span>
  - 执行完成之后可以直接在命令行输入 `create-new-page` 即可(拥有命令自动补全)