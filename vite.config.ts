/*
 * @Author: zhangyang
 * @Date: 2021-02-24 11:28:17
 * @LastEditTime: 2022-11-04 12:09:11
 * @Description: 配置文件
 */
import { defineConfig, ConfigEnv, UserConfigExport, loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import legacy from '@vitejs/plugin-legacy';
import Unocss from 'unocss/vite';
import AutoImport from 'unplugin-auto-import/vite';
import AutoComopnents from 'unplugin-vue-components/vite';
import { VantResolver } from 'unplugin-vue-components/resolvers';
// 获取所有需要暴露给自动导入的方法名称
import * as youngMethods from './src/young-common-expose';

import { resolve } from 'path';
// https://vitejs.dev/config/
export default ({ mode, command }: ConfigEnv): UserConfigExport => {
  // 解析 env 文件
  const root = process.cwd();
  const viteEnv = loadEnv(mode, root);

  return defineConfig({
    // hash模式可以相对路径，history模式必须使用绝对路径
    base: '/',
    resolve: {
      alias: {
        '@': resolve(__dirname, './src'),
        '/components': resolve(__dirname, './src/components'),
        '/views': resolve(__dirname, './src/views'),
        './young-common-expose': resolve(__dirname, './src/young-common-expose.ts'),
      },
    },
    plugins: [
      vue(),
      AutoComopnents({
        dirs: ['./src/components'],
        dts: './src/auto-components.d.ts',
        resolvers: [VantResolver()],
      }),
      AutoImport({
        dts: './src/auto-imports.d.ts',
        imports: [
          'vue',
          'vue-router',
          {
            vant: ['Dialog', 'Notify', 'Toast'],
            './young-common-expose': Object.keys(youngMethods),
          },
        ],
      }),
      vueJsx(),
      Unocss(),
      // 设置为 false 不生成同名 polyfill 文件，打包速度翻倍；移动端必须开启，保证更好的浏览器兼容
      legacy({ renderLegacyChunks: true }),
    ],
    server: {
      open: true,
      port: 3000,
      /**
       * 本地代理服务器
       */
      proxy: {
        [process.env.VITE_BASE_HTTP as string]: {
          target: process.env.VITE_BASE_HTTP,
          changeOrigin: true,
          // 此处替换的字符会拼接于真实请求之后，按需修改
          rewrite: (path) => 'http://localhost',
        },
      },
    },
    logLevel: 'warn',
    build: {
      target: 'es2015',
      // sourcemap: true
    },
  });
};
