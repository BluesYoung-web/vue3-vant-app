/*
 * @Author: zhangyang
 * @Date: 2021-09-18 09:55:01
 * @LastEditTime: 2021-09-18 09:57:38
 * @Description:
 */
export const sleep = async (n: number) => new Promise((resolve) => setTimeout(resolve, n * 1000));
