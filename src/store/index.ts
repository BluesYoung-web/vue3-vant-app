/*
 * @Author: zhangyang
 * @Date: 2021-09-15 19:48:28
 * @LastEditTime: 2021-09-18 10:32:51
 * @Description: 存储管理
 */
import { useSessionStorage } from '@vueuse/core';

enum SessionKeys {
  tabbar标签 = '__young_h5_tabbar_index',
  用户信息 = '__young_h5_user_info',
  静态信息配置 = '__young_h5_static_json',
}

export const tabBarIndex = useSessionStorage<number>(SessionKeys.tabbar标签, 0);

export const userInfo = useSessionStorage<UserInfo>(
  SessionKeys.用户信息,
  {} as unknown as UserInfo,
);

export const conf = useSessionStorage<Record<string, any>>(SessionKeys.静态信息配置, {});
