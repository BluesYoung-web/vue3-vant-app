/// <reference types="vite/client" />

declare module '*.vue' {
  import { DefineComponent } from 'vue';
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

interface ImportMetaEnv {
  /**
   * HTTP 请求地址
   */
  VITE_BASE_HTTP: string;
  /**
   * 是否启用 VConsole 控制台
   */
  VITE_ENABLE_VCONSOLE?: boolean;
}