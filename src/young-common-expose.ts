/*
 * @Author: zhangyang
 * @Date: 2021-08-27 15:32:52
 * @LastEditTime: 2021-09-18 12:01:02
 * @Description: 统一暴露给自动导入插件使用
 */
export { deepClone } from './util/deepClone';
export { isArray } from './util/isType';
export { isJsonStr } from './util/valid';
export { sleep } from './util/sleep';
