/*
 * @Author: zhangyang
 * @Date: 2021-09-09 14:26:58
 * @LastEditTime: 2021-09-18 14:43:22
 * @Description: 用户登录相关接口
 */
import { Params } from './_config';
import { requestWithoutToken, basicRequest } from '../util/request';

export const login = async (args: Form_Login) => {
  const task = 1;
  const { tel: login_name, passwd: login_code } = args;
  const params = { com: Params.com, task, login_name, login_code };
  // return await requestWithoutToken(params);
  return Promise.resolve({ aid: 4, token: 'dsadasewewe' });
};

export const get_verify_code = async () => {};

export const mod_passwd = async (args: Form_Reset_Passwd) => {};

export const register = async (args: Form_Register) => {};
