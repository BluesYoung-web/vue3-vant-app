/*
 * @Author: zhangyang
 * @Date: 2021-12-13 10:21:39
 * @LastEditTime: 2021-12-13 12:11:28
 * @Description: 表头
 */
import type { PropType } from 'vue';
export default defineComponent({
  props: {
    head: { type: Object as PropType<TableHeadItem[]>, required: true },
  },
  setup(props) {
    return () => (
      <thead>
        <tr>
          {props.head.map((item) => (
            <th class="p-2 font-bold text-center">{item.label}</th>
          ))}
        </tr>
      </thead>
    );
  },
});
