/*
 * @Author: zhangyang
 * @Date: 2021-12-13 10:40:39
 * @LastEditTime: 2021-12-13 12:12:25
 * @Description: 表格主体
 */
import type { PropType } from 'vue';
export default defineComponent({
  props: {
    head: { type: Object as PropType<TableHeadItem[]>, required: true },
    body: { type: Object as PropType<TableDataItem[]>, required: true },
  },
  setup(props) {
    const defaultStyles = {
      bgcolor: '#F1F3F7',
      color: '#78838F',
      bgcolor_gradient: `linear-gradient(to right, #E2EBF4, #CBDCF2, #E2EBF4)`,
    };
    const generateTd = (row: TableDataItem) =>
      props.head.map((cell) => (
        <td class="text-center p-2 border-none">{row[String(cell.prop)]}</td>
      ));
    const generateTr = () =>
      props.body.map((row, index) => (
        <tr
          style={{
            backgroundColor: defaultStyles.bgcolor,
            backgroundImage: index % 2 === 0 ? defaultStyles.bgcolor_gradient : 'none',
            color: defaultStyles.color,
          }}
          class="border-none"
        >
          {generateTd(row)}
        </tr>
      ));
    return () => <tbody>{generateTr()}</tbody>;
  },
});
