/*
 * @Author: zhangyang
 * @Date: 2021-12-13 10:13:47
 * @LastEditTime: 2021-12-13 12:11:03
 * @Description: 表格列宽(样式)
 */
import type { PropType } from 'vue';
export default defineComponent({
  props: {
    head: { type: Object as PropType<TableHeadItem[]>, required: true },
  },
  setup(props) {
    return () => (
      <colgroup>
        {props.head.map((item) => (
          <col
            style={{
              width: item?.width ?? 'auto',
            }}
          />
        ))}
      </colgroup>
    );
  },
});
