/*
 * @Author: zhangyang
 * @Date: 2021-09-18 12:19:11
 * @LastEditTime: 2021-09-18 14:06:18
 * @Description: 静态配置(本地开发使用)
 */
export const conf = {
  cdn_url: `/`,
  poster: `poster.png`,
  xie_yi:
    `
<h3 class="text-center">服务协议</h3>  
` +
    `
<span class="ml-4">我是需要遵守的协议内容</span>
`.repeat(200),
  yin_si_zheng_ce:
    `
<h3 class="text-center">隐私政策</h3>  
` +
    `
<span class="ml-4">我是隐私</span>
  `.repeat(200),
  guan_yu_wo_men:
    `
<h3 class="text-center">关于我们</h3>  
` +
    `
<span class="ml-4">有关于你，绝口不提，没关系</span>
  `.repeat(100),
};
