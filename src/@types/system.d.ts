/*
 * @Author: zhangyang
 * @Date: 2021-09-09 11:53:16
 * @LastEditTime: 2021-09-18 14:11:04
 * @Description: 
 */
interface UserKey {
  aid: number;
  token: string;
}

interface UserInfo {
  autoid: number;
  tel: string;
}