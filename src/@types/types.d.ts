/*
 * @Author: zhangyang
 * @Date: 2021-09-09 14:48:03
 * @LastEditTime: 2021-09-18 14:09:13
 * @Description: 
 */
interface Form_Login {
  tel: string;
  passwd: string;
}

interface Form_Register extends Form_Login {
  code: string;
}

interface Form_Reset_Passwd {
  tel: string;
  code: string;
  passwd: string;
  passwd_again: string;
}