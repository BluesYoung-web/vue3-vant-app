/*
 * @Author: zhangyang
 * @Date: 2021-09-09 09:10:07
 * @LastEditTime: 2022-05-22 15:58:10
 * @Description: 项目入口文件
 */
// polyfill
import 'core-js/stable';
import 'regenerator-runtime/runtime';
// 统一浏览器样式
import '@unocss/reset/tailwind.css';
import 'uno.css';

// auto-import 自动导入的 api 目前不能同时导入样式
import 'vant/es/toast/style/index';
import 'vant/es/dialog/style/index';
import 'vant/es/notify/style/index';

// 引入路由
import Router from './route/index';
import { createApp } from 'vue';
import App from './App.vue';
// 引入权限控制
import { getToken } from './util/auth';
import { generateUserInfo } from './util/generateUserInfo';
import './permission';

import { conf as fake } from './dev-static-config';
import { conf } from './store';

(async () => {
  // 根据环境决定是否启用 vconsole
  if (import.meta.env.PROD && import.meta.env.VITE_ENABLE_VCONSOLE) {
    const VConsole = await (await import('vconsole')).default;
    new VConsole();
  }
  // 获取配置文件
  conf.value = fake;
  // conf.value = await (await axios.get('/config.json')).data;
  // 设定视口高度，防止软键盘影响布局
  const metaElement = document.querySelector('#viewportMeta');
  metaElement?.setAttribute(
    'content',
    `maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0,viewport-fit=cover,height=${window.innerHeight}`,
  );
  // 如果有 cookie， 拉取新的用户信息
  if ((getToken() as UserKey).token) {
    generateUserInfo();
  }
  const app = createApp(App);
  // 使用路由
  app.use(Router);
  app.mount('#app');
})();
