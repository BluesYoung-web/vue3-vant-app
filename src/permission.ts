/*
 * @Author: zhangyang
 * @Date: 2021-09-09 11:58:16
 * @LastEditTime: 2021-09-18 10:48:58
 * @Description: 权限控制
 */
import router, { commonRoutes } from './route/index';
import NProgress from 'nprogress';

import { getToken } from './util/auth';

NProgress.configure({
  showSpinner: false,
});

// const hasPermission = (route: string) => {
//   const roleRoute = getRoleRoute().concat(getCommonRoutes());
//   return roleRoute.includes(route);
// }

/**
 * 前置导航守卫
 */
router.beforeEach(async (to, from, next) => {
  NProgress.start();

  // 此处添加鉴权，必须登录之后才能操作
  if (!getToken().token && !commonRoutes.includes(to.path)) {
    await Dialog.alert({
      message: '请先去登录',
    }).catch(() => null);
    next({
      name: `login`,
      query: {
        redirect: to.path,
      },
    });

    NProgress.done();
  }
  // else if (!hasPermission(to.path)) {
  //   // 具体页面权限 map 判断
  //   Toast.fail('暂时无权访问该页面，请勿进行恶意操作');
  //   next(from.path);
  // }
  else {
    next();
  }
});

/**
 * 后置导航守卫
 */
router.afterEach(() => {
  NProgress.done();
});
